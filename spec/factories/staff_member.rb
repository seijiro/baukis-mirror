FactoryGirl.define do
  factory :staff_member do
    sequence(:email){ |n| "member#{n}@example.com" }
      familly_name '山田'
      given_name '多呂'
      familly_name_kana 'ヤマダ'
      given_name_kana 'タロウ'
      password 'pw'
      start_date {Date.yesterday}
      end_date nil
      suspended false
  end
end